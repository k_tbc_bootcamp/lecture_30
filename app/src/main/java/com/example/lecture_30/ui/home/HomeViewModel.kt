package com.example.lecture_30.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    private val number : MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    fun initialize() {
        number.value = 0
    }

    fun returnNumber() : MutableLiveData<Int> {
        return number
    }

    fun add() {
        number.value = number.value!!.plus(1)
    }

    fun subtract() {
        number.value = number.value!!.minus(1)
    }
}