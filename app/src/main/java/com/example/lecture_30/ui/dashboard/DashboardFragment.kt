package com.example.lecture_30.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.lecture_30.R
import com.example.lecture_30.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_dashboard.*

class DashboardFragment : BaseFragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        dashboardViewModel = viewModel as DashboardViewModel
        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
            dashboardFragmentTextView.text = it.toString()
        })
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_dashboard
    }

    override fun getViewModelClass(): ViewModel {
        return DashboardViewModel()
    }
}
