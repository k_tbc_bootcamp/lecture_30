package com.example.lecture_30.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.lecture_30.R
import com.example.lecture_30.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : BaseFragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        homeViewModel = viewModel as HomeViewModel

        homeViewModel.initialize()
        homeViewModel.returnNumber().observe(viewLifecycleOwner, Observer {
            numberTextView.text = it.toString()
        })
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            homeFragmentTextView.text = it
        })

        itemView.addButton.setOnClickListener {
            homeViewModel.add()
        }

        itemView.subtractButton.setOnClickListener {
            homeViewModel.subtract()
        }


    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_home
    }

    override fun getViewModelClass(): ViewModel {
        return HomeViewModel()
    }
}
