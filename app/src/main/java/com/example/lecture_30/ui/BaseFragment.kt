package com.example.lecture_30.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

abstract class BaseFragment: Fragment() {

    protected lateinit var itemView: View
    protected lateinit var viewModel: ViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(getViewModelClass()::class.java)
        itemView = inflater.inflate(getLayoutResource(), container, false)
        start(inflater, container, savedInstanceState)
        return itemView
    }

    abstract fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )

    abstract fun getLayoutResource(): Int

    abstract fun getViewModelClass(): ViewModel
}