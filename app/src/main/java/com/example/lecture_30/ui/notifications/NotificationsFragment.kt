package com.example.lecture_30.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.lecture_30.R
import com.example.lecture_30.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationsFragment : BaseFragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        notificationsViewModel = viewModel as NotificationsViewModel
        notificationsViewModel.text.observe(viewLifecycleOwner, Observer {
            notificationsFragmentTextView.text = it.toString()
        })
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_notifications
    }

    override fun getViewModelClass(): ViewModel {
        return NotificationsViewModel()
    }


}
