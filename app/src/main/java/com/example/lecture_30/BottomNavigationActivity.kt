package com.example.lecture_30

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager.widget.ViewPager
import com.example.lecture_30.ui.FragmentViewPagerAdapter
import com.example.lecture_30.ui.dashboard.DashboardFragment
import com.example.lecture_30.ui.home.HomeFragment
import com.example.lecture_30.ui.notifications.NotificationsFragment
import kotlinx.android.synthetic.main.activity_bottom_navigation.*

class BottomNavigationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)

        init()
    }

    private fun init() {
        val fragments = mutableListOf<Fragment>()
        fragments.add(HomeFragment())
        fragments.add(DashboardFragment())
        fragments.add(NotificationsFragment())

        nav_view_pager.adapter = FragmentViewPagerAdapter(supportFragmentManager, fragments)

        nav_view_pager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                nav_view.menu.getItem(position).isChecked = true
            }
        })

        nav_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> nav_view_pager.currentItem = 0
                R.id.navigation_dashboard -> nav_view_pager.currentItem = 1
                R.id.navigation_notifications -> nav_view_pager.currentItem = 2
            }
            true
        }

        nav_view_pager.offscreenPageLimit = 5
    }
}
